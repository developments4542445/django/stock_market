# Stock Market App

Stock market app created using Django framework

This project is based in:
- Python 3.10.9
- Django
- Stocks APIs
- Database

Libraries and others:
- Django==3.2.5
- Bootstrap: https://getbootstrap.com/
- Stock API page: https://iexcloud.io/console/home
- Database

Steps procedure:
- Define quantity of apps
- Define and design quantity of html files to use
- Link all html files
- Include base file
- Include bootstrap CSS
- Working with API requests
- Including 'Search' button
- Including tickers and its values to database
- Removing data from database
- Including Django messages
- Including tables