from django.shortcuts import render, redirect
import requests
import json
from quotes.models import Stock
from quotes.forms import StockForm
from django.contrib import messages


def home(request):
    if request.method == "POST":
        ticker = request.POST['ticker']

        api_request = requests.get("https://api.iex.cloud/v1/data/core/quote/" + ticker +
                                   "?token=pk_1e2914c26c7448849a911064dc34a6e7")

        try:
            api = json.loads(api_request.content)
            if api[0] is None:
                return render(request, 'quotes/home.html', {'api': "Error"})
        except (Exception,) as _:
            api = "Error"
        return render(request, 'quotes/home.html', {'api': api})
    else:
        return render(request, 'quotes/home.html', {'ticker': "Enter a Ticker Symbol Above"})


def about(request):
    params = {}

    return render(request, 'quotes/about.html', params)


def add_stock(request):
    api = []

    if request.method == "POST":
        form = StockForm(request.POST or None)

        if form.is_valid():
            form.save()
            messages.success(request, "Stock has been added!")
            return redirect('add_stock')
    else:
        ticker = Stock.objects.all()
        output = []

        for ticker_item in ticker:
            api_request = requests.get("https://api.iex.cloud/v1/data/core/quote/" + str(ticker_item) +
                                       "?token=pk_1e2914c26c7448849a911064dc34a6e7")

            try:
                api = json.loads(api_request.content)
                output.append(api[0])
                if api[0] is None:
                    raise Exception("api exception")
            except (Exception,) as _:
                output.append("Error")

        return render(request, 'quotes/add_stock.html', {'ticker': ticker, "output": output})


def delete(request, stock_id):
    item = Stock.objects.get(pk=stock_id)
    item.delete()
    messages.success(request, "Stock has been deleted")

    return redirect(delete_stock)


def delete_stock(request):
    ticker = Stock.objects.all()

    return render(request, 'quotes/delete_stock.html', {"ticker": ticker})
